# @design-view/dom

## 1.0.2

### Patch Changes

- release
- Updated dependencies
  - @design-view/string@1.0.2
  - @design-view/is@1.0.2
