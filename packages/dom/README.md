# @design-view/dom

收集各种操作dom的方法

## 安装

```shell
pnpm add @design-view/dom

```

## 使用

### EMS

```ts
import { findIndex } from '@design-view/dom'

```

### CMD

```js
const helper = require('@design-view/dom')

```

## 注意

由于是封装收集的函数，所以不排除有额外的第三方库
