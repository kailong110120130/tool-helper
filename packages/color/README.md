# @design-view/color

收集各种操作颜色的方法

## 安装

```shell
pnpm add @design-view/color

```

## 使用

### EMS

```ts
import { isHexColor } from '@design-view/color'

```

### CMD

```js
const helper = require('@design-view/color')

```

## 注意

由于是封装收集的函数，所以不排除有额外的第三方库
