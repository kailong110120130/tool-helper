# @design-view/url

收集各种操作url的方法

## 安装

```shell
pnpm add @design-view/url

```

## 使用

### EMS

```ts
import { getUrlQuery } from '@design-view/url'

```

### CMD

```js
const helper = require('@design-view/url')

```

## 注意

由于是封装收集的函数，所以不排除有额外的第三方库
