# @design-view/tree

收集各种操作树形数据的方法

## 安装

```shell
pnpm add @design-view/tree

```

## 使用

### EMS

```ts
import { filter } from '@design-view/tree'

```

### CMD

```js
const helper = require('@design-view/tree')

```

## 注意

由于是封装收集的函数，所以不排除有额外的第三方库
