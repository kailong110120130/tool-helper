# @design-view/date

收集各种操作时间的方法

## 安装

```shell
pnpm add @design-view/date

```

## 使用

### EMS

```ts
import { formatTime } from '@design-view/date'

```

### CMD

```js
const helper = require('@design-view/date')

```

## 注意

由于是封装收集的函数，所以不排除有额外的第三方库
