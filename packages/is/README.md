# @design-view/is

收集各种判断方法

## 安装

```shell
pnpm add @design-view/is

```

## 使用

### EMS

```ts
import { isString } from '@design-view/is'

```

### CMD

```js
const helper = require('@design-view/is')

```

## 注意

由于是封装收集的函数，所以不排除有额外的第三方库
