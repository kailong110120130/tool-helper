# @design-view/string

收集各种操作字符串方法

## 安装

```shell
pnpm add @design-view/string

```

## 使用

### EMS

```ts
import { underlineToHump } from '@design-view/string'

```

### CMD

```js
const helper = require('@design-view/string')

```

## 注意

由于是封装收集的函数，所以不排除有额外的第三方库
