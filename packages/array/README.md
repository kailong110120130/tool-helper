# @design-view/array

收集各种数组方法

## 安装

```shell
pnpm add @design-view/array

```

## 使用

### EMS

```ts
import { findIndex } from '@design-view/array'

```

### CMD

```js
const helper = require('@design-view/array')

```

## 注意

由于是封装收集的函数，所以不排除有额外的第三方库
